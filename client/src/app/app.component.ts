import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable()
export class AppComponent {
  title = 'datetime-client';

  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'yyyy-MM-dd hh:mm',
    defaultOpen: true
  };

  submittedDate: Date = null;

  constructor(private http: HttpClient) {
  }

  public onDateSelect(event) {
    this.date = event;
  }

  public submit() {
    this.submittedDate = null;

    const date = moment(this.date).toISOString(true);

    this.http.post('http://localhost:59598/api/appointment', {
      startsAt: date,
      clientId: uuid(),
      serviceId: uuid(),
      employeeId: uuid()
    }).toPromise()
      .then((resp: any) => {
        this.submittedDate = moment(resp.startsAt).toDate();
      });
  }
}
