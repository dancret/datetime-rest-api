﻿using System;
using System.Diagnostics;
using DateTime.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace DateTime.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentController : ControllerBase
    {
        // POST api/values
        [HttpPost]
        public PostAppointmentModel Post([FromBody] PostAppointmentModel appointment)
        {
            Console.WriteLine($"appointment.StartsAt: {appointment.StartsAt}");
            Debug.WriteLine($"appointment.StartsAt: {appointment.StartsAt}");
            Trace.WriteLine($"appointment.StartsAt: {appointment.StartsAt}");

            return appointment;
        }
    }
}
