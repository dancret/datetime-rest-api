﻿using System;

namespace DateTime.Api.Models
{
    public class PostAppointmentModel
    {
        public System.DateTime StartsAt { get; set; }

        public Guid ClientId { get; set; }

        public Guid ServiceId { get; set; }

        public Guid EmployeeId { get; set; }
    }
}